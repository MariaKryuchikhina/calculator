import java.util.Scanner;

public class CalculatorMortgage {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);

        System.out.print("Введите стоимость недвижимости: ");
        int cost = scan.nextInt();

        System.out.print("Введите первоначальный взнос: ");
        int contribution = scan.nextInt();

        System.out.print("Введите срок кредита: ");
        int term = scan.nextInt();

        System.out.print("Введите процентную ставку: ");
        double rate = scan.nextDouble();

        scan.close();

        MonthlyPayment play = new MonthlyPayment();
        play.calculator(cost, contribution, term, rate);

    }

    public static class MonthlyPayment {
        int cost, contribution, term;
        double rate;

        public void calculator(int cost, int contribution, int term, double rate){
            this.cost = cost;
            this.contribution = contribution;
            this.rate = rate;
            this.term = term;
            //расчет общей суммы с учетом первоначального взноса
            cost -= contribution;

            double scale = Math.pow(10, 6);
            //расчет ежемесячной ставки
            double rateM = Math.ceil((rate/12/100)*scale)/scale;
            System.out.println("Ежемесячная ставка: " + rateM);
            //расчет общей ставки
            double rateO = Math.ceil((Math.pow((1+rateM), (term * 12)))*scale)/scale;
            System.out.println("Общая ставка: " + rateO);
            //расчет ежемесячного платежа
            int paymentM = (int)Math.round( cost * rateM * rateO / (rateO - 1));
            System.out.println("Ежемесячный платеж: " + paymentM);
            //расчет переплаты
            int overpayment = paymentM*(term*12) - cost;
            System.out.println("Переплата: " + overpayment);
            //расчет общей суммы выплат
            int sum = cost + overpayment;
            System.out.println("Общая сумма выплат: " + sum);

            //String[] month = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};
            for(int i=1; i <= term*12; i++){
                double sc = Math.pow(10, 2);
                //расчет процентной части
                double partPercent = Math.ceil((cost * rateM) * sc) / sc;
                //расчет оснвной части
                double partMain = Math.ceil((paymentM - partPercent) * sc) / sc;
                //расчет остатка долга
                cost -= partMain;
                if (cost < 0) {
                    cost = 0;
                }
                System.out.println("Месяц: " + i + "   Процентная часть: " + partPercent + "    Основная часть: " + partMain + "  Остаток долга: " + cost);

            }
        }
    }
}
